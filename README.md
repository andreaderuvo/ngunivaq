# How To

`npm install`  
`ng serve`

# Deploy to HEROKU

1. Iscriversi a Heroku
2. Creare una App in Heroku
3. Installare le seguenti librerie:  
    `npm install -g heroku`  
    `npm install express`  
4. modificare il package.json in  
`"postinstall": "ng build --aot --prod"`,  
    `"start": "node server.js"`
5. Bisogna aggiungere un nuovo remote origin:  
`heroku git:clone -a ngunivaq`
6. `git add .`  
`git commit -am "make it better`  
`git push heroku master`  
 
