export const environment = {
  production: true,
  test: false,
  read_posts_url: 'https://jsonplaceholder.typicode.com/posts',
  insert_post_url: 'https://jsonplaceholder.typicode.com/posts'
};
