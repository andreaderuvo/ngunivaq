import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'alternativecase'
})
export class AlternativecasePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let array = value.split('');
    for (let i = 0; i < array.length; i++) {
      if (i % 2 == 0) {
        array[i] = array[i].toUpperCase();
      } else {
        array[i] = array[i].toLowerCase();
      }
    }
    return array.join('');
  }

}
