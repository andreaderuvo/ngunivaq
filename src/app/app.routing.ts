import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./pages/login/login.component";
import { WelcomeComponent } from "./pages/welcome/welcome.component";
import { AboutComponent } from "./pages/about/about.component";
import { AdminComponent } from "./pages/admin/admin.component";
import { NotFoundComponent } from "./layout/not-found/not-found.component";
import { UnauthorizedComponent } from "./layout/unauthorized/unauthorized.component";
import { IsLoggedInGuard } from "./guards/is-logged-in.guard";
import { PostsComponent } from "./pages/posts/posts.component";

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'welcome',
        component: WelcomeComponent
    },
    {
        path: 'posts',
        component: PostsComponent,
        canActivate: [IsLoggedInGuard]
    },
    {
        path: '',
        component: WelcomeComponent
    },
    {
        path: 'about',
        component: AboutComponent
    }, {
        path: 'admin',
        component: AdminComponent,
        canActivate: [IsLoggedInGuard]
    },
    {
        path: '404',
        component: NotFoundComponent
    },
    {
        path: '401',
        component: UnauthorizedComponent
    },
    {
        path: '**',
        redirectTo: '/404',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }