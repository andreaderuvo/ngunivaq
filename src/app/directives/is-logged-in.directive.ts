import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { LoginService } from '../pages/login/login.service';

@Directive({
  selector: '[isLoggedIn]'
})
export class IsLoggedInDirective {

  @Input("isLoggedIn") isLoggedIn: boolean;

  constructor(protected loginService: LoginService,
    protected templateRef: TemplateRef<any>,
    protected viewContainer: ViewContainerRef) {

    this.loginService.userSubject.subscribe((value : boolean) => {
      this.updateView();
    });
  }

  ngOnInit(): void {
    this.updateView();
  }

  updateView(): void {
    this.viewContainer.clear();
    if (this.loginService.isLoggedIn() == this.isLoggedIn) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

}
