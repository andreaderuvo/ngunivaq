import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../pages/posts/post';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(protected httpClient : HttpClient) { 
  }

  readPosts() : Observable<any> {
    return this.httpClient.get(environment.read_posts_url);
  }

  insertPost(post : Post) : Observable<any> {
    return this.httpClient.post(environment.insert_post_url, post);
  }
}
