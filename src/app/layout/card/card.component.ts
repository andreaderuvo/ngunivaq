import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input('title') title : string;
  @Input('subTitle') subTitle : string;
  @Input('description') description : string;
  @Input('link1') link1 : string;
  @Input('link2') link2 : string;
ù
  constructor() { }

  cardClick() {
    alert("CARD CLICKED!!!");
  }

  ngOnInit() {
  }

}
