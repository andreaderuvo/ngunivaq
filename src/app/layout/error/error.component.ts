import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  @Input('code') code : string;
  @Input('message') message : string;

  constructor(protected router : Router) { }

  ngOnInit() {
  }

  backToHome() {
    this.router.navigate(['/']);
  }

}
