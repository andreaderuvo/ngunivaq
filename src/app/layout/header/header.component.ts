import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../pages/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(protected router : Router, protected loginService : LoginService) { }

  ngOnInit() {
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/']);
  }

  get user() {
    return this.loginService.user;
  }
}
