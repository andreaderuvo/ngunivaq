import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { environment } from '../environments/environment';

declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit, AfterViewInit {
  ngOnDestroy(): void {
    console.log("ONDESTROY");
  }

  ngAfterViewInit(): void {
    console.log("AFTERVIEWINIT");
  }

  ngOnInit(): void {
    console.log("ONINIT");
  }

  constructor() {
    console.log("CONSTRUCTOR");
  }

  get env() {
    return environment;
  }
}
