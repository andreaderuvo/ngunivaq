export const USERS: any = [
    {
        email: 'admin@aDMIN.com',
        password: 'admin',
        role: 'ADMIN',
        name: 'Admin',
        surname: 'Admin'
    },
    {
        email: 'user@user.com',
        password: 'user',
        role: 'USER',
        name: 'User',
        surname: 'User'
    }
]