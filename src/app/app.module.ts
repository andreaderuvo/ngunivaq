import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { LoginComponent } from './pages/login/login.component';
import { TodoComponent } from './pages/todo/todo.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { AboutComponent } from './pages/about/about.component';
import { AppRoutingModule } from './app.routing';
import { LoginService } from './pages/login/login.service';
import { AdminComponent } from './pages/admin/admin.component';
import { IsLoggedInDirective } from './directives/is-logged-in.directive';
import { CardComponent } from './layout/card/card.component';
import { NotFoundComponent } from './layout/not-found/not-found.component';
import { UnauthorizedComponent } from './layout/unauthorized/unauthorized.component';
import { ErrorComponent } from './layout/error/error.component';
import { HttpClientModule } from '@angular/common/http';
import { PostsComponent } from './pages/posts/posts.component';
import { PostComponent } from './pages/posts/post/post.component';
import { AlternativecasePipe } from './pipes/alternativecase.pipe';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    TodoComponent,
    WelcomeComponent,
    AboutComponent,
    AdminComponent,
    IsLoggedInDirective,
    CardComponent,
    NotFoundComponent,
    UnauthorizedComponent,
    ErrorComponent,
    PostsComponent,
    PostComponent,
    AlternativecasePipe
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
