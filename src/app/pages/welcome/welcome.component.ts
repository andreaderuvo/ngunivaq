import { Component, OnInit } from '@angular/core';
import { PageComponent } from '../page/page.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent extends PageComponent implements OnInit {

  ngOnInit() {
  }

}
