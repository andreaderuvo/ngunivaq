import { Component, OnInit, OnDestroy } from '@angular/core';

export abstract class PageComponent implements OnDestroy {
  ngOnDestroy(): void {
    console.log("ONDESTROY");
  }

  constructor() { }

}
