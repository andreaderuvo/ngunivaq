import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { PageComponent } from '../page/page.component';
import { CardComponent } from '../../layout/card/card.component';

declare let $ : any;

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent extends PageComponent implements OnInit,AfterViewInit {
  
  @ViewChild('card1') card1 : CardComponent;

  ngAfterViewInit(): void {
    /* $("#card-1").click((event) => {
      alert("CLICK ON CARD 1");
    }); */

    /* alert(this.card1.description); */
  }

  ngOnInit() {
  }



}
