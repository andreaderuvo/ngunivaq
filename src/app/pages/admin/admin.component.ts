import { Component, OnInit } from '@angular/core';
import { PageComponent } from '../page/page.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent extends PageComponent implements OnInit {

  ngOnInit() {
  }

}
