import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Login } from './login';
import { Router } from '@angular/router';
import { PageComponent } from '../page/page.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends PageComponent implements OnInit {

  loginForm: FormGroup;
  loginFailed: boolean = false;

  constructor(protected router: Router, formBuilder: FormBuilder, protected loginService: LoginService) {
    super();
    this.loginForm = formBuilder.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
  }

  reset(event) {
    event.preventDefault();
    this.loginForm.reset();
    this.loginFailed = false;
  }

  submit() {
    if (this.loginForm.valid) {
      this.loginFailed = false;
      let login: Login = {
        email: this.loginForm.controls.email.value,
        password: this.loginForm.controls.password.value
      }
      let isLoggedIn: boolean = this.loginService.login(login);
      if (isLoggedIn) {
        this.router.navigate(['/admin']);
      } else {
        setTimeout(() => {
          this.loginFailed = true;
        }, 2000);
      }
    }
  }

}
