import { Injectable } from '@angular/core';
import { USERS } from '../../db';
import { Login } from './login';
import { BehaviorSubject } from 'rxjs';

declare let $ : any;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userSubject : BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor() { }

  login(login : Login) : boolean {
    let user = USERS.find(obj => obj.email.toLowerCase() === login.email.toLowerCase() && obj.password === login.password);
    if (user) {
      let userClone = $.extend({}, user);
      delete userClone.password;
      localStorage.setItem("user", JSON.stringify(userClone));
      this.userSubject.next(true);
      return true;
    }
    this.userSubject.next(false);
    return false;
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(false);
  }

  isLoggedIn() {
    return localStorage.getItem('user') ? true : false;
  }

  get user() {
    return JSON.parse(localStorage.getItem('user'));
  }
}
