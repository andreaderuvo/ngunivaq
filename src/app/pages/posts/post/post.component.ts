import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input('title') title : string;
  @Input('body') body : string;

  constructor() { }

  ngOnInit() {
  }

  get background() {
    let colors = ['bg-primary', 'bg-secondary', 'bg-success', 'bg-danger', 
      'bg-warning', 'bg-info', 'bg-light', 'bg-dark', 'bg-white'];
    return colors[Math.floor(Math.random() * colors.length)];
  }

}
