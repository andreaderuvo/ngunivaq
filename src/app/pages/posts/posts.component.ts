import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { Post } from './post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: Array<Post>;

  constructor(protected restService: RestService) { }

  ngOnInit() {
    this.restService.readPosts().subscribe(data => {
      this.posts = data;
      console.log(data);
    });
  }

  insertPost() {
    let post: Post = {
      userId: 100,
      title: 'title',
      body: 'body'
    }

    this.restService.insertPost(post).subscribe(data => {
      if (data) {
        alert("Success!!");
      } else {
        alert('error');
      }
    });
  }
}
